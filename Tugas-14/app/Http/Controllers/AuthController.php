<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register()
    {
        return view('halaman.register');
    }

    public function welcome(Request $request)
    {
        $fname = $request['fname'];
        $lname = $request['lname'];
        $gender = $request['gender'];
        $nationality = $request['nationality'];
        $language = $request['language'];
        $bio = $request['bio'];

        return view('halaman.welcome', ['fname' => $fname, 'lname' => $lname, 'gender' => $gender, 'nationality' => $nationality, 'language' => $language, 'bio' => $bio]);
    }
}
