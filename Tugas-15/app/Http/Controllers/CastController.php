<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required|max:45',
                'umur' => 'required|integer',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong!',
                'nama.max' => 'Nama tidak boleh melebihi 45 karakter!',
                'umur.required' => 'Umur tidak boleh kosong!',
                'umur.integer' => 'Umur harus berupa angka!',
                'bio.required' => 'Bio tidak boleh kosong!'
            ]
        );

        DB::table('cast')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return redirect('/cast')->with(['success' => 'Cast berhasil ditambahkan!']);
    }

    public function index()
    {
        $cast = DB::table('cast')->get();
        return view('cast.index', ['cast' => $cast]);
    }

    public function show($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.detail', ['cast' => $cast]);
    }

    public function edit($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.edit', ['cast' => $cast]);
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required|max:45',
                'umur' => 'required|integer',
                'bio' => 'required'
            ],
            [
                'nama.required' => 'Nama tidak boleh kosong!',
                'nama.max' => 'Nama tidak boleh melebihi 45 karakter!',
                'umur.required' => 'Umur tidak boleh kosong!',
                'umur.integer' => 'Umur harus berupa angka!',
                'bio.required' => 'Bio tidak boleh kosong!'
            ]
        );

        DB::table('cast')->where('id', $id)->update(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio'],
                'updated_at' => now()
            ]
        );

        return redirect('/cast')->with(['success' => 'Cast berhasil diubah!']);
    }

    public function delete($id)
    {
        $cast = DB::table('cast')->find($id);
        return view('cast.delete', ['cast' => $cast]);
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', $id)->delete();
        return redirect('/cast')->with(['success' => 'Cast berhasil dihapus!']);
    }
}
