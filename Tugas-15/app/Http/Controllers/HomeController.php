<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('halaman.home');
    }

    public function table()
    {
        return view('halaman.table');
    }

    public function datatable()
    {
        return view('halaman.datatable');
    }
}
