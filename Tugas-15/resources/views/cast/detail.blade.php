@extends('layouts.master')

@section('title')
    Halaman Detail Cast
@endsection

@section('sub-title')
    Detail Cast
@endsection

@section('content')
    <h1>{{ $cast->nama }}</h1>
    <h3>Umur: {{ $cast->umur }}</h3>
    <p>{{ $cast->bio }}</p>
@endsection
