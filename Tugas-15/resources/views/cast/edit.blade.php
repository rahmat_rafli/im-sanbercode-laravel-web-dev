@extends('layouts.master')

@section('title')
    Halaman Create Cast
@endsection

@section('sub-title')
    Create Cast
@endsection

@section('content')
    <form action="/cast/{{ $cast->id }}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>Nama</label>
            <input type="text" class="form-control @error('nama') is-invalid @enderror" name="nama"
                value="{{ old('nama', $cast->nama) }}">
            @error('nama')
                <div class="form-text text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Umur</label>
            <input type="text" class="form-control @error('umur') is-invalid @enderror" name="umur"
                value="{{ old('umur', $cast->umur) }}">
            @error('umur')
                <div class="form-text text-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="form-group">
            <label>Bio</label>
            <textarea name="bio" class="form-control @error('bio') is-invalid @enderror">{{ old('bio', $cast->bio) }}</textarea>
            @error('bio')
                <div class="form-text text-danger">{{ $message }}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
@endsection
