@extends('layouts.master')

@section('title')
    Halaman Register
@endsection

@section('sub-title')
    Sign Up Form
@endsection

@section('content')
    <form action="/welcome" method="post">
        @csrf
        <div class="form-group">
            <label>First name:</label>
            <input type="text" name="fname" class="form-control" />
        </div>
        <div class="form-group">
            <label>Last name:</label>
            <input type="text" name="lname" class="form-control" />
        </div>

        <div class="form-group">
            <label>Gender:</label>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="inlineRadio1" value="1">
                <label class="form-check-label" for="inlineRadio1">Male</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="inlineRadio2" value="2">
                <label class="form-check-label" for="inlineRadio2">Female</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" id="inlineRadio3" value="3">
                <label class="form-check-label" for="inlineRadio3">Other</label>
            </div>
        </div>

        <div class="form-group">
            <label>Nationality:</label>
            <select name="nationality" class="form-control">
                <option value="1">Indonesian</option>
                <option value="2">Singaporean</option>
                <option value="3">Malaysian</option>
                <option value="4">Australian</option>
            </select>
        </div>

        <div class="form-group">
            <label>Language Spoken:</label>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck1">
                <label class="form-check-label" for="defaultCheck1">
                    Bahasa Indonesia
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck2">
                <label class="form-check-label" for="defaultCheck2">
                    English
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="" id="defaultCheck3">
                <label class="form-check-label" for="defaultCheck3">
                    Other
                </label>
            </div>
        </div>
        <div class="form-group">
            <label>Bio:</label>
            <textarea name="bio" cols="30" rows="10" class="form-control"></textarea>
        </div>
        <button type="submit" class="btn btn-primary">Sign Up</button>
    </form>
@endsection
