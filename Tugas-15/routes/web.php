<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index']);
Route::get('/register', [AuthController::class, 'register']);
Route::post('/welcome', [AuthController::class, 'welcome']);
Route::get('/table', [HomeController::class, 'table']);
Route::get('/data-tables', [HomeController::class, 'datatable']);

// index menuju ke halaman cast
Route::get('/cast', [CastController::class, 'index']);
// menuju ke halaman create cast
Route::get('/cast/create', [CastController::class, 'create']);
Route::post('/cast', [CastController::class, 'store']);
// menuju ke halaman detail cast
Route::get('/cast/{id}', [CastController::class, 'show']);
// menuju ke halaman edit cast
Route::get('/cast/{id}/edit', [CastController::class, 'edit']);
Route::put('/cast/{id}', [CastController::class, 'update']);
// menghapus data cast
Route::get('/cast/{id}/delete', [CastController::class, 'delete'])->name('delete');
Route::delete('/cast/{id}', [CastController::class, 'destroy']);
